package forward_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	forward "."
	"../comm"
	"../config"
	"../link/hosts"
)

func TestDisplayRules(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestAdd")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	testMux := http.NewServeMux()

	comm.OverrideMux(testMux)
	comm.RegisterDefaults()

	cfg := config.TestDir(dir)
	if err := hosts.Init(cfg); err != nil {
		t.Fatalf("hosts.Init() failed: %v", err)
	}
	if err := forward.Init(cfg); err != nil {
		t.Fatalf("forward.Init() failed: %v", err)
	}

	ts := httptest.NewServer(testMux)
	defer ts.Close()

	hosts.Add("rule-test-host")
	forward.Add("rule-test-host", &forward.Rule{Local: "8000", Remote: "9000"})
	forward.Add("rule-test-host", &forward.Rule{Local: "8012", Remote: "9012", Bind: "192.168.1.200"})

	resp, err := http.Get(ts.URL + "/_rules")
	if err != nil {
		t.Errorf("http.Get(%q) returned an error: %v", ts.URL+"/_rules", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("ioutil.ReadAll returned an error: %v", err)
	}

	if got, want := string(body), "rule-test-host: 8000 -> 9000: (192.168.1.200) 8012 -> 9012\n"; got != want {
		t.Errorf("Rules response should be %s, but was %s",
			want,
			got,
		)
	}
}
