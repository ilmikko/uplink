package forward

import (
	"fmt"

	"../config"
)

var (
	Rules map[string][]*Rule
)

type Rule struct {
	Bind   string
	Local  string
	Remote string
}

func (forward *Rule) String() string {
	bind := forward.Bind
	if bind != "" {
		bind += ":"
	}

	local := forward.Local
	remote := forward.Remote

	return fmt.Sprintf("%s%s:%s", bind, local, remote)
}

func NewRule(bind, local, remote string) *Rule {
	return &Rule{
		Bind:   bind,
		Local:  local,
		Remote: remote,
	}
}

func For(host string) []*Rule {
	forwards, ok := Rules[host]
	if !ok {
		return []*Rule{}
	}
	return forwards
}

func Add(host string, rules ...*Rule) {
	oldRules, ok := Rules[host]
	if !ok {
		oldRules = []*Rule{}
	}

	Rules[host] = append(oldRules, rules...)
}

func RulesString(host string) []string {
	forwards := For(host)

	out := []string{}
	for _, f := range forwards {
		bind := f.Bind
		if bind != "" {
			bind += ":"
		}

		local := f.Local
		remote := f.Remote

		out = append(out, "-L", fmt.Sprintf("%s%s:localhost:%s", bind, local, remote))
	}
	return out
}

func Reset() {
	Rules = map[string][]*Rule{}
}

func Init(cfg *config.Config) error {
	Reset()

	if f := cfg.RuleFile; f != "" {
		_, r, err := Load(f)
		if err != nil {
			return fmt.Errorf("loading rules from file %q: %v", f, err)
		}
		Rules = r
	}

	httpInit()
	return nil
}
