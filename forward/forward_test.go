package forward_test

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	forward "."
	"../unix"
)

func TestAdd(t *testing.T) {
	forward.Reset()

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})

	if got, want := forward.For("some-host"), []*forward.Rule{}; !reflect.DeepEqual(want, got) {
		t.Errorf("Forwards were not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}

	forward.Add("some-host", &forward.Rule{Local: "4211", Remote: "4212"})

	if got, want := forward.For("some-host"), []*forward.Rule{
		{Local: "4211", Remote: "4212"},
	}; !reflect.DeepEqual(want, got) {
		t.Errorf("Forwards were not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}

	forward.Add("some-host", &forward.Rule{Local: "12211", Remote: "4212"})

	if got, want := forward.For("some-host"), []*forward.Rule{
		{Local: "4211", Remote: "4212"},
		{Local: "12211", Remote: "4212"},
	}; !reflect.DeepEqual(want, got) {
		t.Errorf("Forwards were not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}
}

func TestRulesString(t *testing.T) {
	forward.Reset()

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})

	forward.Add("rule-test-host", &forward.Rule{Local: "1000", Remote: "2000"})

	if got, want := forward.RulesString("rule-test-host"), []string{
		"-L", "1000:localhost:2000",
	}; !reflect.DeepEqual(want, got) {
		t.Errorf("Rules were not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}

	forward.Add("rule-test-host", &forward.Rule{Local: "1500", Remote: "2500", Bind: "192.146.1.1"})

	if got, want := forward.RulesString("rule-test-host"), []string{
		"-L", "1000:localhost:2000",
		"-L", "192.146.1.1:1500:localhost:2500",
	}; !reflect.DeepEqual(want, got) {
		t.Errorf("Rules were not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}
}
