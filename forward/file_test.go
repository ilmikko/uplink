package forward_test

import (
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	forward "."
)

func TestParseRule(t *testing.T) {
	rules := []struct {
		rule string
		want *forward.Rule
	}{
		{
			rule: "1234",
			want: &forward.Rule{
				Remote: "1234",
				Local:  "1234",
			},
		},
		{
			rule: "1234:1234",
			want: &forward.Rule{
				Remote: "1234",
				Local:  "1234",
			},
		},
		{
			rule: "2222:1111",
			want: &forward.Rule{
				Local:  "2222",
				Remote: "1111",
			},
		},
		{
			rule: "192.168.1.1:2222:1111",
			want: &forward.Rule{
				Bind:   "192.168.1.1",
				Local:  "2222",
				Remote: "1111",
			},
		},
	}

	for _, r := range rules {
		got, err := forward.ParseRule(r.rule)
		if err != nil {
			t.Errorf("Parse error: %v", err)
		}
		if want := r.want; !reflect.DeepEqual(want, got) {
			t.Errorf("Rule was not parsed as expected:\nwant: %v\ngot : %v",
				want,
				got,
			)
		}
	}
}

func TestSaveLoad(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestSaveLoad")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	hostFile := dir + "/test_config_hosts"

	h := map[string]struct{}{}
	r := map[string][]*forward.Rule{}

	forward.Save(hostFile, h, r)
	h, r, err = forward.Load(hostFile)
	if err != nil {
		t.Errorf("Error from hosts.Load: %v", err)
	}
	if got, want := h, map[string]struct{}{}; !reflect.DeepEqual(want, got) {
		t.Errorf("Loaded data was not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}

	h = map[string]struct{}{
		"test-host-1": struct{}{},
	}

	forward.Save(hostFile, h, r)
	h, r, err = forward.Load(hostFile)
	if err != nil {
		t.Errorf("Error from hosts.Load: %v", err)
	}
	if got, want := h, map[string]struct{}{
		"test-host-1": struct{}{},
	}; !reflect.DeepEqual(want, got) {
		t.Errorf("Loaded data was not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}

	h = map[string]struct{}{
		"test-host-1": struct{}{},
		"test-host-2": struct{}{},
		"test-host-4": struct{}{},
	}

	forward.Save(hostFile, h, r)
	h, r, err = forward.Load(hostFile)
	if err != nil {
		t.Errorf("Error from hosts.Load: %v", err)
	}
	if got, want := h, map[string]struct{}{
		"test-host-1": struct{}{},
		"test-host-2": struct{}{},
		"test-host-4": struct{}{},
	}; !reflect.DeepEqual(want, got) {
		t.Errorf("Loaded data was not as expected:\nwant: %v\ngot : %v",
			want,
			got,
		)
	}
}
