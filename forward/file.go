package forward

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func ParseRule(rule string) (*Rule, error) {
	arr := strings.Split(rule, ":")
	switch len(arr) {
	case 3:
		return &Rule{
			Bind:   arr[0],
			Local:  arr[1],
			Remote: arr[2],
		}, nil
	case 2:
		return &Rule{
			Local:  arr[0],
			Remote: arr[1],
		}, nil
	case 1:
		return &Rule{
			Local:  arr[0],
			Remote: arr[0],
		}, nil
	default:
		return nil, fmt.Errorf("Parsing error, please tokenize your rule as `bind:local:remote`, `local:remote` or `localandremote`.")
	}
}

func parseLine(text string) (string, []*Rule, error) {
	fields := strings.Fields(text)
	if len(fields) == 0 {
		return "", nil, fmt.Errorf("Line is empty")
	}

	host, fields := fields[0], fields[1:]
	forwards := []*Rule{}

	for _, f := range fields {
		rule, err := ParseRule(f)
		if err != nil {
			return "", nil, err
		}

		forwards = append(forwards, rule)
	}

	return host, forwards, nil
}

func Save(file string, hosts map[string]struct{}, forwards map[string][]*Rule) error {
	f, err := os.Create(file + ".part")
	if err != nil {
		return err
	}
	defer f.Close()

	for host := range hosts {
		rules := []string{}
		fwds := forwards[host]
		for _, rule := range fwds {
			rules = append(rules, rule.String())
		}
		str := fmt.Sprintf("%s %s\n",
			host,
			strings.Join(rules, " "),
		)
		f.WriteString(str)
	}

	if err := os.Rename(file+".part", file); err != nil {
		return fmt.Errorf("Could not overwrite file %q: %v", file, err)
	}
	return nil
}

func Load(file string) (map[string]struct{}, map[string][]*Rule, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	hosts := map[string]struct{}{}
	rules := map[string][]*Rule{}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		if text == "" {
			continue
		}

		h, r, err := parseLine(text)
		if err != nil {
			return nil, nil, fmt.Errorf("Failed to parse line %q of %q: %v", text, file, err)
		}

		hosts[h] = struct{}{}
		rules[h] = r
	}
	if err := scanner.Err(); err != nil {
		return nil, nil, err
	}

	return hosts, rules, nil
}
