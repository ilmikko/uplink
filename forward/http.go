package forward

import (
	"fmt"
	"net/http"

	"../comm"
)

func Format() string {
	str := ""

	for host, fwds := range Rules {
		forwards := ""
		for _, fwd := range fwds {
			bind := fwd.Bind
			if bind != "" {
				bind = fmt.Sprintf("(%s) ", bind)
			}
			local := fwd.Local
			remote := fwd.Remote
			forwards += fmt.Sprintf(": %s%s -> %s", bind, local, remote)
		}
		str += fmt.Sprintf("%s%s\n", host, forwards)
	}

	return str
}

func httpInit() {
	comm.Register("_rules", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, Format())
	})
}
