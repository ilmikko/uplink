package unix

var (
	OVERRIDE_EXEC func(cmd string, args ...string) string
)

func OverrideExec(f func(cmd string, args ...string) string) {
	OVERRIDE_EXEC = f
}
