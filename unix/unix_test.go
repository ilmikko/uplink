package unix_test

import (
	"bytes"
	"os"
	"reflect"
	"regexp"
	"strings"
	"testing"
	"time"

	unix "."
)

// TODO: Test overrides

func TestCommandStrings(t *testing.T) {
	testCases := []struct {
		cmd  *unix.Cmd
		want []string
	}{
		{
			cmd: &unix.Cmd{
				Name: "whoami",
				Args: []string{},
				Opts: []string{},
			},
			want: []string{
				"whoami",
			},
		},
		{
			cmd: &unix.Cmd{
				Name: "echo",
				Args: []string{"hello world"},
				Opts: []string{},
			},
			want: []string{
				"echo",
				"hello world",
			},
		},
		{
			cmd: &unix.Cmd{
				Name: "echo",
				Args: []string{"hello", "world"},
				Opts: []string{},
			},
			want: []string{
				"echo",
				"hello",
				"world",
			},
		},
		{
			cmd: &unix.Cmd{
				Name: "echo",
				Args: []string{"hello", "world"},
				Opts: []string{"-e"},
			},
			want: []string{
				"echo",
				"-e",
				"hello",
				"world",
			},
		},
		{
			cmd: &unix.Cmd{
				Name: "ssh",
				Args: []string{"user@localhost"},
				Opts: []string{"-p 2200"},
			},
			want: []string{
				"ssh",
				"-p 2200",
				"user@localhost",
			},
		},
	}

	for _, tc := range testCases {
		got := tc.cmd.Strings()
		want := tc.want

		if !reflect.DeepEqual(got, want) {
			t.Errorf("Command string output not as expected:\n got: %q\nwant: %q",
				got,
				want,
			)
		}
	}
}

func TestDaemon(t *testing.T) {
	cmd := unix.Command("echo", "one")

	stdout := &bytes.Buffer{}
	stderr := &bytes.Buffer{}

	if _, err := unix.Daemon(cmd, stdout, stderr); err != nil {
		t.Errorf("unix.Daemon: %v", err)
	}

	time.Sleep(10 * time.Millisecond)

	want := "one\n"

	if got := stdout.String(); want != got {
		t.Errorf("Daemon STDOUT is not as expected:\n got: %q\nwant: %q",
			got,
			want,
		)
	}
}

func TestExec(t *testing.T) {
	out, err := unix.Exec("mktemp", "/tmp/testExec.XXXX")
	if err != nil {
		t.Errorf("unix.Exec returned an error: %v", err)
	}

	file := strings.Trim(out, "\n ")
	defer os.Remove(file)

	fileregexp := regexp.MustCompile("^\\/tmp\\/testExec\\..{4}$")
	if !fileregexp.Match([]byte(file)) {
		t.Errorf("mktemp file did not match regexp: %s", file)
	}

	if _, err := os.Stat(file); os.IsNotExist(err) {
		t.Errorf("Expected file to exist, but it did not: %s", file)
	}
}

func TestExecFail(t *testing.T) {
	_, err := unix.Exec("nonexistent-command", "some", "arguments")
	if err == nil {
		t.Errorf("unix.Exec(nonexistent-command) should have returned an error but did not")
	}
}
