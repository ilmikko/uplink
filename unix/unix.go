package unix

import (
	"bytes"
	"fmt"
	"os/exec"
)

type Cmd struct {
	Name string
	Opts []string
	Args []string
}

func (c *Cmd) Arg(args ...string) {
	c.Args = append(c.Args, args...)
}

func (c *Cmd) Opt(opts ...string) {
	c.Opts = append(c.Opts, opts...)
}

func (c *Cmd) Strings() []string {
	s := []string{c.Name}

	s = append(s, c.Opts...)
	s = append(s, c.Args...)

	return s
}

func Command(cmd string, args ...string) *Cmd {
	return &Cmd{
		Name: cmd,
		Opts: []string{},
		Args: args,
	}
}

func Daemon(c *Cmd, stdout, stderr *bytes.Buffer) (*exec.Cmd, error) {
	strs := c.Strings()
	command, args := strs[0], strs[1:]

	if OVERRIDE_EXEC != nil {
		stdout.WriteString(
			OVERRIDE_EXEC(command, args...),
		)
		return &exec.Cmd{}, nil
	}

	cmd := exec.Command(command, args...)

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	if err := cmd.Start(); err != nil {
		return &exec.Cmd{}, err
	}

	return cmd, nil
}

// TODO: Convert to new Cmd format
func Exec(command string, args ...string) (string, error) {
	if OVERRIDE_EXEC != nil {
		return OVERRIDE_EXEC(command, args...), nil
	}

	cmd := exec.Command(command, args...)

	out, err := cmd.Output()
	if err != nil {
		return "", fmt.Errorf("Command failed: %v (%s): %v", err, cmd, string(out))
	}

	return string(out), nil
}
