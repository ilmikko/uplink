package main

import (
	"fmt"
	"log"

	"./comm"
	"./config"
	"./forward"
	"./link"
	"./link/hosts"
)

func mainErr() error {
	cfg, err := config.Init()
	if err != nil {
		return fmt.Errorf("config.Init(): %v", err)
	}

	if err := comm.Init(cfg); err != nil {
		return fmt.Errorf("comm.Init(): %v", err)
	}

	log.Printf("Running as %s on %s on port %s",
		cfg.User,
		cfg.SelfAddress,
		cfg.SelfPort,
	)

	if err := forward.Init(cfg); err != nil {
		return fmt.Errorf("forward.Init(): %v", err)
	}

	if err := hosts.Init(cfg); err != nil {
		return fmt.Errorf("hosts.Init(): %v", err)
	}

	for host := range forward.Rules {
		hosts.Add(host)
	}

	if err := link.Init(cfg); err != nil {
		return fmt.Errorf("link.Init(): %v", err)
	}

	return comm.Listen(cfg.SelfPort)
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("Fatal: %v", err)
	}
}
