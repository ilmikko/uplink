PACKAGES=\
	./config \
	./unix \
	./comm \
	./forward \
	./link/health \
	./link/hosts/keys/authkeys \
	./link/hosts/keys/knownhosts \
	./link/hosts/keys \
	./link/hosts \
	./link/ssh/health/status \
	./link/ssh/health \
	./link/ssh \
	./link/sshfs/directorylink/linksocket \
	./link/sshfs/directorylink \
	./link/sshfs \
	./link \

test:
	go test $(PACKAGES)

coverage:
	go test $(PACKAGES) \
	-coverprofile=c.out
	go tool cover -html=c.out
