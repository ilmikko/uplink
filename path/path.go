package path

import (
	"io"
	"os"
)

func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}

	if !s.IsDir() {
		return false
	}

	return true
}

func IsEmptyDir(path string) bool {
	d, err := os.Open(path)
	if err != nil {
		return false
	}
	defer d.Close()

	if _, err = d.Readdirnames(1); err == io.EOF {
		return true
	}
	return false

}
