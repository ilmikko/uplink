package config

import (
	"fmt"
)

func (cfg *Config) PublicKeyFile() string {
	return fmt.Sprintf("%s/%s", cfg.KeyDir, "link.key.pub")
}

func (cfg *Config) PrivateKeyFile() string {
	return fmt.Sprintf("%s/%s", cfg.KeyDir, "link.key")
}
