package config

import (
	"fmt"
	"os"
	"os/user"

	"../path"
)

type Config struct {
	SelfAddress string
	SelfPort    string

	User string
	Uid  string

	KeyDir  string
	HomeDir string

	HostFile  string
	KeyFile   string
	RuleFile  string
	SshfsFile string

	SingleLink bool
}

func (cfg *Config) GetEnv() error {
	cfg.HomeDir = os.Getenv("HOME")

	{
		u, err := user.Current()
		if err != nil {
			return fmt.Errorf("user.Current(): %v", err)
		}

		cfg.User = u.Username
		cfg.Uid = u.Uid
	}

	return nil
}

func (cfg *Config) Validate() error {
	if cfg.Uid == "0" {
		return fmt.Errorf("You never should run uplink as root! Instead, create another user (such as `uplink` or `link`) to run as.")
	}

	if cfg.SelfAddress == "" {
		// TODO: Only for double-link hosts.
		return fmt.Errorf("Please set a discoverable self address via the --address command line variable.")
	}

	if !path.IsDir(cfg.KeyDir) {
		return fmt.Errorf("Expected key dir %q to be a directory, but it's not!", cfg.KeyDir)
	}
	if !path.IsDir(cfg.HomeDir) {
		return fmt.Errorf("Expected home %q to be a directory, but it's not!", cfg.HomeDir)
	}
	return nil
}

func New() *Config {
	c := &Config{
		KeyDir:     ".",
		HomeDir:    ".",
		SelfPort:   "22000",
		SingleLink: false,
	}

	return c
}

func Init() (*Config, error) {
	cfg := New()

	cfg.GetEnv()
	if f := ConfigFile(); f != "" {
		if err := cfg.GetFile(f); err != nil {
			return nil, err
		}
	}
	cfg.GetFlags()

	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}
