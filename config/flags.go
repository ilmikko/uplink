package config

import (
	"flag"
)

var (
	singleLink = flag.Bool("single", false, "Use single links instead of double links.")
)

var (
	address    = flag.String("address", "", "An address that points to this host.")
	configFile = flag.String("config", "", "File to load uplink configuration from.")
	ruleFile   = flag.String("rulefile", "", "File to load host forwarding rules from.")
	hostFile   = flag.String("hostfile", "", "File to load host key rules from.")
	keyFile    = flag.String("keyfile", "", "File to load authentication keys from.")
	sshfsFile  = flag.String("sshfsfile", "", "File to load SSHFS rules from.")
	port       = flag.String("port", "", "Port to listen on for link requests, shared across nodes.")
	homeDir    = flag.String("homedir", "", "Directory where the .ssh directory is located.")
	keyDir     = flag.String("keydir", "", "Directory to store keys in.")
)

func boolOverride(p, o bool) bool {
	if o {
		return o
	}
	return p
}

func stringOverride(p, o string) string {
	if o != "" {
		return o
	}
	return p
}

func (cfg *Config) GetFlags() {
	cfg.SingleLink = boolOverride(cfg.SingleLink, *singleLink)

	cfg.RuleFile = stringOverride(cfg.RuleFile, *ruleFile)
	cfg.HostFile = stringOverride(cfg.HostFile, *hostFile)
	cfg.KeyFile = stringOverride(cfg.KeyFile, *keyFile)
	cfg.SshfsFile = stringOverride(cfg.SshfsFile, *sshfsFile)

	cfg.HomeDir = stringOverride(cfg.HomeDir, *homeDir)
	cfg.KeyDir = stringOverride(cfg.KeyDir, *keyDir)
	cfg.SelfAddress = stringOverride(cfg.SelfAddress, *address)
	cfg.SelfPort = stringOverride(cfg.SelfPort, *port)
}

func ConfigFile() string {
	flag.Parse()
	return *configFile
}
