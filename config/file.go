package config

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var (
	SINGLE_LINK = false
)

func (cfg *Config) GetFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		fields := strings.Fields(s.Text())
		if len(fields) == 0 {
			continue
		}

		cmd, fields := fields[0], fields[1:]
		switch cmd {
		case "ADDRESS":
			cfg.SelfAddress = strings.Join(fields, " ")
		case "HOME":
			cfg.HomeDir = strings.Join(fields, " ")
		case "KEYDIR":
			cfg.KeyDir = strings.Join(fields, " ")
		case "PORT":
			cfg.SelfPort = strings.Join(fields, " ")
		case "RULEFILE":
			cfg.RuleFile = strings.Join(fields, " ")
		case "HOSTFILE":
			cfg.HostFile = strings.Join(fields, " ")
		case "KEYFILE":
			cfg.KeyFile = strings.Join(fields, " ")
		case "SSHFSFILE":
			cfg.SshfsFile = strings.Join(fields, " ")
		case "SINGLELINK":
			cfg.SingleLink = true
		case "DOUBLELINK":
			cfg.SingleLink = false
		default:
			return fmt.Errorf("Unknown field in config file: %s", cmd)
		}
	}

	return nil
}
