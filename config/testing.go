package config

import ()

func TestDir(dir string) *Config {
	c := New()

	c.KeyDir = dir
	c.HomeDir = dir

	return c
}
