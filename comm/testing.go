package comm

import (
	"net/http"
	"net/url"
)

var (
	OVERRIDE_URL  string
	OVERRIDE_HOST string
)

func OverrideURL(uri string) {
	u, _ := url.Parse(uri)
	OVERRIDE_HOST = u.Host
}

func OverrideMux(m *http.ServeMux) {
	SERVE_MUX = m
}
