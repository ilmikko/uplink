package port

import (
	"strconv"

	"../../config"
)

var (
	NEXT_PORT   = 0
	HOSTS_PORTS = map[string]int{}
)

func nextPort() int {
	NEXT_PORT += 1
	return NEXT_PORT - 1
}

// TODO: Fail fast when we are retrieving a port for a host that should not exist.
func For(host string) int {
	port, ok := HOSTS_PORTS[host]
	if ok {
		return port
	}
	port = nextPort()
	HOSTS_PORTS[host] = port
	return port
}

func Init(cfg *config.Config) error {
	p, err := strconv.Atoi(cfg.SelfPort)
	if err != nil {
		return err
	}

	NEXT_PORT = p + 1
	return nil
}
