package comm_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	comm "."
)

func TestGet(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "URL:%s", r.URL.Path)
		}),
	)
	defer ts.Close()

	comm.OverrideURL(ts.URL)

	want := "URL:/some/url"
	got, gotErr := comm.Get(
		url.URL{Path: "/some/url"},
	)
	if gotErr != nil {
		t.Errorf("Want response %s but failed with %v",
			want,
			gotErr,
		)
	}
	if got != want {
		t.Errorf("Want response %s but it was %s",
			want,
			got,
		)
	}
}

func TestRegister(t *testing.T) {
	testmux := http.NewServeMux()

	ts := httptest.NewServer(testmux)
	defer ts.Close()

	comm.OverrideMux(testmux)
	comm.OverrideURL(ts.URL)

	comm.Register("_path", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "URL:%s", r.URL.Path)
	})

	want := "URL:/_path"
	got, gotErr := comm.Get(
		url.URL{Path: "/_path"},
	)
	if gotErr != nil {
		t.Errorf("Want response %s but failed with %v",
			want,
			gotErr,
		)
	}
	if got != want {
		t.Errorf("Want response %s but it was %s",
			want,
			got,
		)
	}
}

func TestRegisterArgs(t *testing.T) {
	testmux := http.NewServeMux()

	ts := httptest.NewServer(testmux)
	defer ts.Close()

	comm.OverrideMux(testmux)
	comm.OverrideURL(ts.URL)

	comm.RegisterArgs("_argpath", func(args []string) string {
		switch len(args) {
		case 1:
			return fmt.Sprintf("Arg was %v", args[0])
		case 2:
			return fmt.Sprintf("Args were %v and %v", args[0], args[1])
		default:
			return ""
		}
	})

	testCases := []struct {
		path string
		want string
	}{
		{
			path: "/_argpath/1/2/3",
			want: "",
		},
		{
			path: "/_argpath/1/2",
			want: "Args were 1 and 2",
		},
		{
			path: "/_argpath/1",
			want: "Arg was 1",
		},
		{
			path: "/_argpath/",
			want: "",
		},
		{
			path: "/_argpath",
			want: "",
		},
		{
			path: "/_argpath/%3D/%2F",
			want: "Args were = and /",
		},
	}

	for _, c := range testCases {
		got, gotErr := comm.Get(
			url.URL{Path: c.path},
		)
		if gotErr != nil {
			t.Errorf("Want response %s but failed with %v",
				c.want,
				gotErr,
			)
		}
		if got != c.want {
			t.Errorf("Want response %s but it was %s",
				c.want,
				got,
			)
		}
	}
}

func TestServeRoot(t *testing.T) {
	testMux := http.NewServeMux()

	comm.OverrideMux(testMux)
	comm.RegisterDefaults()

	server := httptest.NewServer(testMux)
	defer server.Close()

	resp, err := http.Get(server.URL + "/")
	if err != nil {
		t.Errorf("GET: %v", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("ReadAll: %v", err)
	}

	want := ""

	if got, want := string(body), want; got != want {
		t.Errorf("Root response should be %q, but was %q",
			want,
			got,
		)
	}
}

func TestServePing(t *testing.T) {
	testMux := http.NewServeMux()

	comm.OverrideMux(testMux)
	comm.RegisterDefaults()

	server := httptest.NewServer(testMux)
	defer server.Close()

	resp, err := http.Get(server.URL + "/_ping")
	if err != nil {
		t.Errorf("GET: %v", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("ReadAll: %v", err)
	}

	want := "PONG\n"

	if got, want := string(body), want; got != want {
		t.Errorf("PING response should be %q, but was %q",
			want,
			got,
		)
	}
}
