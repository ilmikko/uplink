package comm

import (
	"fmt"
	"log"
	"net/http"
)

func Listen(port string) error {
	log.Printf("Server listening on port %s", port)
	return http.ListenAndServe(
		fmt.Sprintf(":%s", port),
		nil,
	)
}
