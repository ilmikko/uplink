package comm

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"../config"
	"./port"
)

var (
	SERVE_MUX = http.DefaultServeMux
)

func Get(url url.URL) (string, error) {
	url.Scheme = "http"
	if OVERRIDE_HOST != "" {
		url.Host = OVERRIDE_HOST
	}

	resp, err := http.Get(url.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func Query(host, path string, args ...string) (string, error) {
	extra := ""
	if len(args) > 0 {
		for i := range args {
			args[i] = url.QueryEscape(args[i])
		}
		extra = "/" + strings.Join(args, "/")
	}
	return Get(url.URL{
		Host: fmt.Sprintf(
			"%s:%v",
			"localhost",
			port.For(host),
		),
		Path: path + extra,
	})
}

func Register(path string, f func(http.ResponseWriter, *http.Request)) {
	SERVE_MUX.HandleFunc(fmt.Sprintf("/%s", path), f)
}

func RegisterArgs(path string, f func(args []string) string) {
	pathRegexp := regexp.MustCompile(
		fmt.Sprintf("\\/%s\\/(.+)", path),
	)

	SERVE_MUX.HandleFunc(
		fmt.Sprintf("/%s/", path),
		func(w http.ResponseWriter, r *http.Request) {
			matches := pathRegexp.FindStringSubmatch(r.URL.EscapedPath())

			if len(matches) < 2 {
				return
			}

			m, err := url.QueryUnescape(matches[1])
			if err != nil {
				return
			}

			args := strings.Split(m, "/")
			for i := range args {
				m, err := url.QueryUnescape(args[i])
				if err != nil {
					return
				}
				args[i] = m
			}

			fmt.Fprintf(w, "%s", f(args))
		},
	)
}

func RegisterDefaults() {
	Register("", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			log.Printf("Unknown request: %s", r.URL.Path)
		}
	})

	Register("_ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "PONG\n")
	})
}

func Init(cfg *config.Config) error {
	if err := port.Init(cfg); err != nil {
		return err
	}
	RegisterDefaults()
	return nil
}
