package hosts

import (
	"fmt"
	"net/http"
	"sort"

	"../../comm"
	"../../comm/port"
	"../ssh/health/status"
	"../sshfs"
)

func FormatSsh() string {
	str := ""

	hosts := []string{}
	for host := range HOSTS {
		hosts = append(hosts, host)
	}
	sort.Slice(hosts, func(i, j int) bool {
		return hosts[i] < hosts[j]
	})

	for _, host := range hosts {
		stat := status.For(host)
		port := port.For(host)
		str += fmt.Sprintf("%s %d %s\n", host, port, stat.String())
	}

	return str
}

func FormatSshfs() string {
	return sshfs.Status()
}

// TODO: Show singly linked hosts on _hosts? Perhaps use info from netstat?
func httpInit() {
	comm.Register("_hosts", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, FormatSsh())
	})

	comm.Register("_dirs", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, FormatSshfs())
	})

	comm.Register("_address", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, CFG.SelfAddress)
	})
}
