package hosts_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"

	hosts "."
	"../../comm"
	"../../config"
	"../../unix"
)

func TestAdd(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestAdd")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})
	comm.OverrideMux(http.NewServeMux())

	cfg := config.TestDir(dir)

	if err := hosts.Init(cfg); err != nil {
		t.Fatalf("hosts.Init: %v", err)
	}

	adds := []struct {
		host string
		want map[string]*hosts.Host
	}{
		{
			host: "test-host",
			want: map[string]*hosts.Host{
				"test-host": hosts.New("test-host"),
			},
		},
		{
			host: "test-host",
			want: map[string]*hosts.Host{
				"test-host": hosts.New("test-host"),
			},
		},
		{
			host: "other-test-host",
			want: map[string]*hosts.Host{
				"test-host":       hosts.New("test-host"),
				"other-test-host": hosts.New("other-test-host"),
			},
		},
	}

	for _, add := range adds {
		hosts.Add(add.host)

		if got, want := hosts.HOSTS, add.want; !reflect.DeepEqual(want, got) {
			t.Errorf("Hosts were not as expected:\nwant: %v\ngot : %v",
				want,
				got,
			)
		}
	}
}

func TestHealth(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestAdd")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "URL:%s", r.URL.Path)
		}),
	)
	defer ts.Close()

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})
	comm.OverrideURL(ts.URL)
	comm.OverrideMux(http.NewServeMux())

	cfg := config.TestDir(dir)
	cfg.SelfAddress = "self"

	if err := hosts.Init(cfg); err != nil {
		t.Fatalf("hosts.Init: %v", err)
	}

	health, err := hosts.Health("test-host")
	if err != nil {
		t.Errorf("hosts.Health() returned an error: %v", err)
	}

	if got, want := health, "URL:/_health/self"; got != want {
		t.Errorf("hosts.Health() should be %s but it was %s",
			want,
			got,
		)
	}
}

func TestKey(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestAdd")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "URL:%s", r.URL.Path)
		}),
	)
	defer ts.Close()

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})
	comm.OverrideURL(ts.URL)
	comm.OverrideMux(http.NewServeMux())

	cfg := config.TestDir(dir)

	if err := hosts.Init(cfg); err != nil {
		t.Fatalf("hosts.Init: %v", err)
	}

	key, err := hosts.Key("test-host")
	if err != nil {
		t.Errorf("hosts.Key() returned an error: %v", err)
	}

	if got, want := key, "URL:/_key"; got != want {
		t.Errorf("hosts.Key() should be %s but it was %s",
			want,
			got,
		)
	}
}

func TestLink(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestLink")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "URL:%s", r.URL.Path)
		}),
	)
	defer ts.Close()

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})
	comm.OverrideURL(ts.URL)
	comm.OverrideMux(http.NewServeMux())

	cfg := config.TestDir(dir)
	cfg.SelfAddress = "self"

	if err := hosts.Init(cfg); err != nil {
		t.Fatalf("hosts.Init: %v", err)
	}

	err = ioutil.WriteFile(cfg.PublicKeyFile(), []byte("PUBLIC KEY"), 0600)
	if err != nil {
		t.Fatalf("Cannot make temporary public key file: %v", err)
	}

	link, err := hosts.Link("test-host")
	if err != nil {
		t.Errorf("hosts.Link() returned an error: %v", err)
	}

	if got, want := link, "URL:/_link/self/PUBLIC+KEY"; got != want {
		t.Errorf("hosts.Link() should be %s but it was %s",
			want,
			got,
		)
	}
}
