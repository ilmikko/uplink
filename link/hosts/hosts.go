package hosts

import (
	"fmt"

	"../../comm"
	"../../config"

	"./keys"
)

var (
	CFG   *config.Config
	HOSTS map[string]*Host
)

type Host struct {
	name string
}

func New(name string) *Host {
	keys.Scan(name)
	return &Host{
		name: name,
	}
}

func Add(name string) *Host {
	host, ok := HOSTS[name]

	if !ok {
		host = New(name)
	}

	HOSTS[name] = host

	return host
}

func Key(host string) (string, error) {
	return comm.Query(host, "_key")
}

func Link(host string) (string, error) {
	Add(host)

	key, err := Key(host)
	if err != nil {
		return "", fmt.Errorf("Could not retrieve key for host: %v", host)
	}

	if err := keys.Authorize(key, host); err != nil {
		return "", fmt.Errorf("Could not authorize key: %v", err)
	}

	return comm.Query(host, "_link", CFG.SelfAddress, keys.Public())
}

func Trust(host, key string) error {
	if err := keys.Trust(host, key); err != nil {
		return err
	}
	return nil
}

func Reset() {
	HOSTS = map[string]*Host{}
}

func Init(cfg *config.Config) error {
	Reset()
	CFG = cfg
	if err := keys.Init(cfg); err != nil {
		return fmt.Errorf("keys.Init(): %v", err)
	}

	httpInit()
	return nil
}
