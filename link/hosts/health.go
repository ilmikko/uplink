package hosts

import (
	"fmt"
	"time"

	"../../comm"
)

const (
	HEALTH_TIMEOUT = 5 * time.Second
)

// Chans don't support multiple values :(
type HealthResponse struct {
	res string
	err error
}

func Health(host string) (string, error) {
	query := make(chan HealthResponse)
	go func() {
		res, err := comm.Query(host, "_health", CFG.SelfAddress)
		hr := HealthResponse{
			res: res,
			err: err,
		}
		query <- hr
	}()

	timeout := time.After(HEALTH_TIMEOUT)

	select {
	case hr := <-query:
		res, err := hr.res, hr.err
		if err != nil {
			return "", err
		}

		return res, nil
	case <-timeout:
		return "", fmt.Errorf("Timed out after %s", HEALTH_TIMEOUT)
	}
}
