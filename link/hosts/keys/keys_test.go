package keys_test

import (
	"bufio"
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	keys "."
	"../../../comm"
	"../../../config"
	"../../../unix"
)

func TestGenerate(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestGenerate")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	unix.OverrideExec(nil)

	if err := keys.Generate(dir + "/test.key"); err != nil {
		t.Errorf("Key generation failed: %v", err)
	}

	// Test that files exist
	if _, err := os.Stat(dir + "/test.key"); os.IsNotExist(err) {
		t.Errorf("Private key not found in %s", dir+"/test.key")
	}
	if _, err := os.Stat(dir + "/test.key.pub"); os.IsNotExist(err) {
		t.Errorf("Public key not found in %s", dir+"/test.key.pub")
	}

	// Test that the files have a correct format
	file, err := os.Open(dir + "/test.key")
	if err != nil {
		t.Errorf("Private key could not be opened: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()

	if got, want := scanner.Text(), "-----BEGIN OPENSSH PRIVATE KEY-----"; got != want {
		t.Errorf("Private key first line should be %s but it was %s",
			want,
			got,
		)
	}

	file, err = os.Open(dir + "/test.key.pub")
	if err != nil {
		t.Errorf("Public key could not be opened: %v", err)
	}
	defer file.Close()

	scanner = bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)
	scanner.Scan()

	if got, want := scanner.Text(), "ssh-rsa"; got != want {
		t.Errorf("Public key first word should be %s but it was %s",
			want,
			got,
		)
	}
}

func TestInit(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestInit")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	unix.OverrideExec(nil)
	comm.OverrideMux(http.NewServeMux())

	cfg := config.TestDir(dir)

	if err := keys.Init(cfg); err != nil {
		t.Errorf("keys.Init() failed: %v", err)
	}

	// Test that the files have a correct format
	file, err := os.Open(cfg.PrivateKeyFile())
	if err != nil {
		t.Errorf("Private key could not be opened: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()

	if got, want := scanner.Text(), "-----BEGIN OPENSSH PRIVATE KEY-----"; got != want {
		t.Errorf("Private key first line should be %s but it was %s",
			want,
			got,
		)
	}
}
