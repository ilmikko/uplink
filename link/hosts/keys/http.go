package keys

import (
	"fmt"
	"log"
	"net/http"

	"../../../comm"
)

func httpInit() {
	comm.Register("_key", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, Public())
	})

	comm.RegisterArgs("_trust", func(args []string) string {
		if len(args) < 2 {
			return ""
		}

		host := args[0]
		key := args[1]

		if err := Trust(host, key); err != nil {
			log.Printf("Trust failure: %v", err)
			return "FAIL"
		}
		return "OK"
	})
}
