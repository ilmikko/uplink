package keys_test

import (
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"."
	"../../../comm"
	"../../../config"
)

func TestPublic(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestPublic")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	comm.OverrideMux(http.NewServeMux())

	cfg := config.TestDir(dir)
	if err := keys.Init(cfg); err != nil {
		t.Errorf("keys.Init: %v", err)
	}

	err = ioutil.WriteFile(cfg.PublicKeyFile(), []byte("PUBLIC KEY"), 0600)
	if err != nil {
		t.Fatalf("Cannot make temporary public key file: %v", err)
	}

	// If file is cached, this should still pass after removing the public key.
	if got, want := keys.Public(), "PUBLIC KEY"; got != want {
		t.Errorf("Public key should be %q but it was %q",
			want,
			got,
		)
	}

	if err := os.Remove(cfg.PublicKeyFile()); err != nil {
		t.Fatalf("Could not remove temporary public key file: %v", err)
	}

	if got, want := keys.Public(), "PUBLIC KEY"; got != want {
		t.Errorf("Public key should be %q but it was %q",
			want,
			got,
		)
	}
}
