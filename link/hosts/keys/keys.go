package keys

import (
	"fmt"
	"log"
	"os"

	"../../../config"
	"../../../unix"

	"./authkeys"
	"./knownhosts"
)

var (
	// TODO: move to config
	KEY_BITS    = 2048
	CFG         *config.Config
	KNOWN_HOSTS *knownhosts.KnownHosts
	AUTH_KEYS   *authkeys.AuthKeys
)

func Authorize(key, host string) error {
	log.Printf("Authorizing key %s for host %s", key, host)
	if err := AUTH_KEYS.Add(host, key); err != nil {
		return fmt.Errorf("Authorize: %v", err)
	}
	return nil
}

func Generate(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Printf("Generating key %q", path)
		out, err := unix.Exec(
			"ssh-keygen",
			fmt.Sprintf("-b %d", KEY_BITS),
			"-N", "",
			"-f", path,
		)
		if err != nil {
			return fmt.Errorf("Failed to generate key %q: %v: %s", path, err, string(out))
		}
		log.Printf("KEY: %s", string(out))
	}
	return nil
}

func Scan(host string) error {
	if err := KNOWN_HOSTS.Get(host).Scan(); err != nil {
		return err
	}
	return nil
}

func Trust(host, key string) error {
	if err := Scan(host); err != nil {
		return fmt.Errorf("Scan(%q): %v", host, err)
	}

	if err := Authorize(key, host); err != nil {
		return fmt.Errorf("Authorize: %v", err)
	}

	return nil
}

func Init(cfg *config.Config) error {
	CFG = cfg

	{
		ak, err := authkeys.Load(cfg.KeyFile)
		if err != nil {
			return fmt.Errorf("Failed to load auth config: %v", err)
		}
		AUTH_KEYS = ak
	}
	{
		kh, err := knownhosts.Load(cfg.HostFile)
		if err != nil {
			return fmt.Errorf("Failed to load host config: %v", err)
		}
		KNOWN_HOSTS = kh
	}

	AUTH_KEYS.Project(
		fmt.Sprintf("%s/%s", cfg.KeyDir, "authorized_keys"),
	)
	KNOWN_HOSTS.Project(
		fmt.Sprintf("%s/%s", cfg.KeyDir, "known_hosts"),
	)

	if err := keyLocalGenerate(); err != nil {
		return fmt.Errorf("Failed to generate local key: %v", err)
	}

	httpInit()
	return nil
}
