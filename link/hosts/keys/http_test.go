package keys_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"

	keys "."
	"../../../comm"
	"../../../config"
	"../../../unix"
)

func TestTrust(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestAuthorize")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	testMux := http.NewServeMux()

	unix.OverrideExec(func(cmd string, args ...string) string {
		switch cmd {
		case "ssh-keyscan":
			host := args[0]
			return strings.Join([]string{
				fmt.Sprintf("# %s:22 SSH-9.9-OpenSSH_9.9", host),
				fmt.Sprintf("%s %s %s",
					host,
					"ssh-rsa",
					fmt.Sprintf("%s-key", host),
				),
			}, "\n")
		}
		return ""
	})
	comm.OverrideMux(testMux)

	cfg := config.TestDir(dir)

	if err := keys.Init(cfg); err != nil {
		t.Errorf("keys.Init() failed: %v", err)
	}

	ts := httptest.NewServer(testMux)
	defer ts.Close()

	_, err = http.Get(ts.URL + "/_trust/test-host/test-key")
	if err != nil {
		t.Errorf("Unknown error: %v", err)
	}

	authKeys := []string{}
	for _, key := range keys.AUTH_KEYS.Keys {
		authKeys = append(authKeys, key)
	}
	got := authKeys

	if got, want := got, []string{"test-key"}; !reflect.DeepEqual(got, want) {
		t.Errorf("Authorized keys file not as expected:\n got: %v\nwant: %v",
			got,
			want,
		)
	}
}
