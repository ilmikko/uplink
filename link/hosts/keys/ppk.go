package keys

import (
	"io/ioutil"
	"log"
)

var (
	PUBLIC_KEY_CACHED = ""
)

func publicKeyCached() string {
	if PUBLIC_KEY_CACHED != "" {
		return PUBLIC_KEY_CACHED
	}

	key, err := ioutil.ReadFile(CFG.PublicKeyFile())
	if err != nil {
		log.Fatalf("Could not read local key file: %v", err)
	}

	PUBLIC_KEY_CACHED = string(key)
	return PUBLIC_KEY_CACHED
}

func keyLocalGenerate() error {
	priv := CFG.PrivateKeyFile()
	if err := Generate(priv); err != nil {
		return err
	}
	log.Printf("Using local key %v", priv)
	return nil
}

func Public() string {
	return publicKeyCached()
}
