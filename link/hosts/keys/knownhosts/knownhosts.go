package knownhosts

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"

	"../../../../unix"
)

const (
	MODE         = 0600
	EDIT_WARNING = "# AUTOMATICALLY GENERATED FILE; DO NOT EDIT MANUALLY!"
)

type KeySet struct {
	Host string
	Keys map[string]string
}

func (ks *KeySet) Add(keyType, key string) error {
	if oldKey, ok := ks.Keys[keyType]; ok {
		if oldKey != key {
			return fmt.Errorf("Host %q key (%s) mismatch!\nOLD: %q\nNEW: %q", ks.Host, keyType, oldKey, key)
		}
		return nil
	}

	ks.Keys[keyType] = key
	return nil
}

func (ks *KeySet) Scan() error {
	log.Printf("Scanning host %q", ks.Host)
	out, err := unix.Exec(
		"ssh-keyscan",
		ks.Host,
	)
	if err != nil {
		return fmt.Errorf("Key scanning failed for host %q: %v: %s", ks.Host, err, out)
	}

	kh, err := parseHostKeys(strings.NewReader(out))
	if err != nil {
		return fmt.Errorf("Error parsing host keys: %v", err)
	}

	hks, ok := kh.HostKeys[ks.Host]
	if !ok {
		return fmt.Errorf("No host keys were found for host %q: %s", ks.Host, out)
	}

	for kt, key := range hks.Keys {
		switch kt {
		case "ssh-rsa":
			ks.Add(kt, key)
		default:
			// TODO: Should this always return an error?
			return fmt.Errorf("Unknown key type: %q", kt)
		}
	}

	return nil
}

func NewKeySet(host string) *KeySet {
	return &KeySet{
		Host: host,
		Keys: map[string]string{},
	}
}

type KnownHosts struct {
	HostKeys map[string]*KeySet
}

func (kh *KnownHosts) AddEmpty(host string) {
	if _, ok := kh.HostKeys[host]; !ok {
		kh.HostKeys[host] = NewKeySet(host)
	}
}

func (kh *KnownHosts) Add(host, kt, key string) error {
	kh.AddEmpty(host)
	return kh.HostKeys[host].Add(kt, key)
}

func (kh *KnownHosts) AddSet(host string, keys *KeySet) error {
	log.Printf("Adding host %q with %d key(s)", host, len(keys.Keys))
	if oldKeys, ok := kh.HostKeys[host]; ok {
		for kt, k := range keys.Keys {
			if err := oldKeys.Add(kt, k); err != nil {
				return fmt.Errorf("Key migration failed: %v", err)
			}
		}
		return nil
	}

	kh.HostKeys[host] = keys
	return nil
}

func (kh *KnownHosts) Get(host string) *KeySet {
	if _, ok := kh.HostKeys[host]; !ok {
		kh.AddEmpty(host)
	}

	ks := kh.HostKeys[host]
	return ks
}

func (kh *KnownHosts) String() string {
	s := []string{}

	for host, keys := range kh.HostKeys {
		for keyType, key := range keys.Keys {
			s = append(s, fmt.Sprintf("%s %s %s\n", host, keyType, key))
		}
	}

	sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })

	return strings.Join(s, "")
}

// Project to a known_hosts file.
func (kh *KnownHosts) Project(file string) error {
	f, err := os.OpenFile(file, os.O_CREATE|os.O_WRONLY, MODE)
	if err != nil {
		return err
	}
	defer f.Close()

	fmt.Fprintf(f, "%s\n", EDIT_WARNING)
	fmt.Fprintf(f, "%s", kh.String())
	return nil
}

func New() *KnownHosts {
	return &KnownHosts{
		HostKeys: map[string]*KeySet{},
	}
}

func parseHostKeys(r io.Reader) (*KnownHosts, error) {
	kh := New()
	s := bufio.NewScanner(r)
	for s.Scan() {
		fields := strings.Fields(s.Text())

		// Lines need to have at least hostname, keytype and key data.
		if len(fields) < 3 {
			continue
		}

		// Comments
		if fields[0][0] == '#' {
			continue
		}

		hostName := fields[0]
		keyType := fields[1]
		keyData := strings.Join(fields[2:], " ")
		if err := kh.Add(hostName, keyType, keyData); err != nil {
			return nil, err
		}
	}

	return kh, nil
}

func Load(file string) (*KnownHosts, error) {
	if file == "" {
		return New(), nil
	}

	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	kh, err := parseHostKeys(f)
	if err != nil {
		return nil, err
	}

	return kh, nil
}
