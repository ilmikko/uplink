package authkeys

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

const (
	MODE         = 0600
	EDIT_WARNING = "# AUTOMATICALLY GENERATED FILE; DO NOT EDIT MANUALLY!"
)

type AuthKeys struct {
	Keys map[string]string
}

func (ak *AuthKeys) Add(host, key string) error {
	if oldKey, ok := ak.Keys[host]; ok {
		if oldKey != key {
			return fmt.Errorf("Host %q key mismatch!\nOLD: %q\nNEW: %q", host, oldKey, key)
		}
		return nil
	}

	ak.Keys[host] = key
	return nil
}

func (ak *AuthKeys) Project(file string) error {
	f, err := os.OpenFile(file, os.O_CREATE|os.O_WRONLY, MODE)
	if err != nil {
		return err
	}
	defer f.Close()

	// TODO: Instead, where to edit?
	fmt.Fprintf(f, "%s\n", EDIT_WARNING)
	fmt.Fprintf(f, "%s", ak.String())
	return nil
}

func (ak *AuthKeys) String() string {
	s := []string{}

	for host, key := range ak.Keys {
		s = append(s, fmt.Sprintf("%s # Uplink for host %q\n", key, host))
	}

	sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })

	return strings.Join(s, "")
}

func New() *AuthKeys {
	return &AuthKeys{
		Keys: map[string]string{},
	}
}

func parseAuthKeys(r io.Reader) (*AuthKeys, error) {
	ak := New()
	s := bufio.NewScanner(r)
	for s.Scan() {
		fields := strings.Fields(s.Text())

		// Lines need to have at least hostname and key data.
		if len(fields) < 2 {
			continue
		}

		// Comments
		if fields[0][0] == '#' {
			continue
		}

		hostName := fields[0]
		keyData := strings.Join(fields[1:], " ")
		if err := ak.Add(hostName, keyData); err != nil {
			return nil, err
		}
	}
	return ak, nil
}

func Load(file string) (*AuthKeys, error) {
	if file == "" {
		return New(), nil
	}

	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	kh, err := parseAuthKeys(f)
	if err != nil {
		return nil, err
	}

	return kh, nil
}
