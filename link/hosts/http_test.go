package hosts_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	hosts "."
	"../../comm"
	"../../config"
)

func TestDisplayHosts(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestAdd")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	testMux := http.NewServeMux()

	comm.OverrideMux(testMux)
	comm.RegisterDefaults()

	cfg := config.TestDir(dir)

	if err := hosts.Init(cfg); err != nil {
		t.Fatalf("hosts.Init() failed: %v", err)
	}

	ts := httptest.NewServer(testMux)
	defer ts.Close()

	hosts.Add("host-test-host")
	hosts.Add("host-test-host2")
	hosts.Add("host-test-host3")

	resp, err := http.Get(ts.URL + "/_hosts")
	if err != nil {
		t.Errorf("http.Get(%s) returned an error: %v", ts.URL+"/_hosts", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("ioutil.ReadAll(...) returned an error: %v", err)
	}

	if got, want := string(body), "host-test-host 1 [1;33mUNKNOWN[m\nhost-test-host2 2 [1;33mUNKNOWN[m\nhost-test-host3 3 [1;33mUNKNOWN[m\n"; got != want {
		t.Errorf("Hosts response should be %s, but was %s",
			want,
			got,
		)
	}
}
