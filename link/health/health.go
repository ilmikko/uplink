package health

// TODO: Test this package alone

import (
	"fmt"
	"log"
	"time"

	"../../comm"
)

const (
	HEALTHCHECK_INTERVAL = 2 * time.Minute
)

type HealthCheckable interface {
	Health(string) bool
	HealthAll() bool

	Repair(string) bool
	RepairAll() bool
}

var (
	CHECKS = []HealthCheckable{}
)

func Register(h HealthCheckable) {
	CHECKS = append(CHECKS, h)
}

// A single health check.
func Check(host string) bool {
	for _, c := range CHECKS {
		if !c.Health(host) {
			return false
		}
	}
	return true
}

func CheckAll() bool {
	for _, c := range CHECKS {
		if !c.HealthAll() {
			return false
		}
	}
	return true
}

func Repair(host string) (success bool) {
	success = true
	for _, c := range CHECKS {
		if !c.Repair(host) {
			success = false
		}
	}
	return success
}

func RepairAll() (success bool) {
	log.Printf("Resetting unhealthy links...")
	success = true
	for _, c := range CHECKS {
		if !c.RepairAll() {
			success = false
		}
	}
	return success
}

func healthCheckRoutine() {
	for {
		if !CheckAll() {
			if !RepairAll() {
				log.Printf("Reset failed.")
			}
		}
		time.Sleep(HEALTHCHECK_INTERVAL)
	}
}

func Init() error {
	comm.RegisterArgs("_verify_all", func(args []string) string {
		if s := RepairAll(); !s {
			return fmt.Sprintf("FAIL")
		}

		return fmt.Sprintf("OK")
	})
	comm.RegisterArgs("_verify", func(args []string) string {
		if len(args) < 1 {
			return ""
		}

		host := args[0]
		if s := Repair(host); !s {
			return fmt.Sprintf("FAIL %s", host)
		}

		return fmt.Sprintf("OK %s", host)
	})

	go healthCheckRoutine()
	return nil
}
