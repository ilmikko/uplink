package sshfs

import (
	"log"
)

func (s *SSHFS) Health(host string) bool {
	dls, ok := s.Hosts[host]
	if !ok {
		return false
	}

	for _, dl := range dls {
		if !dl.Health() {
			return false
		}
	}
	return true
}

func (s *SSHFS) HealthAll() bool {
	for host := range s.Hosts {
		if !s.Health(host) {
			log.Printf("SSHFS host %q reporting unhealthy", host)
			return false
		}
	}
	return true
}

func (s *SSHFS) Repair(host string) bool {
	dls, ok := s.Hosts[host]
	if !ok {
		return true
	}

	if s.Health(host) {
		return true
	}

	for _, dl := range dls {
		if err := dl.Reset(); err != nil {
			log.Printf("Cannot reset %q: %v", host, err)
			return false
		}
	}
	return true
}

func (s *SSHFS) RepairAll() (success bool) {
	success = true
	for host := range s.Hosts {
		if !s.Repair(host) {
			success = false
		}
	}
	return success
}
