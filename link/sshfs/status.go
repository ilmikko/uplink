package sshfs

import (
	"fmt"
)

func (s *SSHFS) Status() string {
	st := ""

	for host, dls := range s.Hosts {
		for _, dl := range dls {
			st += fmt.Sprintf("%s %s\n", host, dl.Status())
		}
	}

	return st
}

func Status() string {
	s := ""

	for _, m := range SSHFS_MODULES {
		s += m.Status()
	}

	return s
}
