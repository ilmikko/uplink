package linksocket

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

const (
	MODE         = 0600
	HEALTHY      = "OK"
	EXEC_TIMEOUT = 2 * time.Second // How long to wait for write/read to last before aborting.
)

func healthCheckWriteRead(sock string) error {
	testfile := fmt.Sprintf("%s/%s", sock, HEALTH_FILE)

	err := ioutil.WriteFile(testfile, []byte(HEALTHY), MODE)
	if err != nil {
		return fmt.Errorf("write: %v", err)
	}
	defer os.Remove(testfile)

	bytes, err := ioutil.ReadFile(testfile)
	if err != nil {
		return fmt.Errorf("read: %v", err)
	}

	if string(bytes) != HEALTHY {
		return fmt.Errorf("Mismatch of data: %q != %q", string(bytes), HEALTHY)
	}
	return nil
}

// The health check tries to write and read in the mount directory, and reports true if it succeeds.
// This will fail in case SSHFS is unmounted because the mode of that directory is 000.
// In case of network errors, the health check will terminate itself after a timeout (and fail).
// This is because the default behaviour in SSHFS is to hang all writes/reads until connection is re-established.
func MountHealthCheck(sock string) error {
	wr := make(chan error)
	go func() { wr <- healthCheckWriteRead(sock) }()

	timeout := time.After(EXEC_TIMEOUT)

	select {
	case <-timeout:
		return fmt.Errorf("WriteRead timed out after %s", EXEC_TIMEOUT)
	case err := <-wr:
		if err != nil {
			return fmt.Errorf("WriteRead: %v", err)
		}
	}
	return nil
}
