package linksocket

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"../../../../comm"
	"../../../../path"
)

const (
	CONNECTION_FILE = "CONNECTION"
	MOUNT_FOLDER    = "RW"
	HEALTH_FILE     = "HEALTHCHECK"
)

// Local folders are never health checked, so this file should not be read in a healthy connection.
func createLocalHealthCheckFile(sock string) error {
	file := fmt.Sprintf("%s/%s", sock, HEALTH_FILE)

	if err := os.Remove(file); err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("Removing previous %q: %v", file, err)
	}

	if err := ioutil.WriteFile(
		file,
		[]byte("FAIL"),
		0444,
	); err != nil {
		return fmt.Errorf("Writing %q: %v", file, err)
	}
	return nil
}

func createConnectionFile(sock, data string) error {
	connection := fmt.Sprintf("%s/%s", sock, CONNECTION_FILE)

	if err := ioutil.WriteFile(
		connection,
		[]byte(
			fmt.Sprintf("%s\n", data),
		),
		0644,
	); err != nil {
		return fmt.Errorf("Writing %q: %v", connection, err)
	}
	return nil
}

func createMountDir(sock string, mode os.FileMode) error {
	mount := fmt.Sprintf("%s/%s", sock, MOUNT_FOLDER)

	if err := os.Mkdir(mount, mode); err != nil {
		if !os.IsExist(err) {
			return fmt.Errorf("Creating %q: %v", mount, err)
		}
		if err := os.Chmod(mount, mode); err != nil {
			return fmt.Errorf("chmod %q: %v", mount, err)
		}
	}
	return nil
}

func Create(sock string) error {
	if !path.IsDir(sock) {
		return fmt.Errorf("Expected link socket to be a directory: %q", sock)
	}

	if err := createLocalHealthCheckFile(sock); err != nil {
		return err
	}
	if err := createConnectionFile(sock, "FAIL"); err != nil {
		return err
	}
	if err := createMountDir(sock, 0000); err != nil {
		return err
	}
	return nil
}

func CreateLocal(sock string) error {
	if !path.IsDir(sock) {
		return fmt.Errorf("Expected link socket to be a directory: %q", sock)
	}

	if err := createConnectionFile(sock, "OK"); err != nil {
		return err
	}
	if err := createMountDir(sock, 0755); err != nil {
		return err
	}
	return nil
}

func VerifyRemote(host, remote string) error {
	res, err := comm.Query(host, "_sshfs_create", remote)
	if err != nil {
		return fmt.Errorf("query: %v", err)
	}

	fields := strings.Fields(res)
	if fields[0] != "OK" {
		return fmt.Errorf("Remote verification failed for host %q: %s", host, res)
	}
	return nil
}

func Init() error {
	comm.RegisterArgs("_sshfs_create", func(args []string) string {
		if len(args) < 1 {
			return ""
		}

		if err := CreateLocal(args[0]); err != nil {
			log.Printf("Failed to create sshfs stub: %v", err)
			return "FAIL"
		}
		return "OK"
	})
	return nil
}
