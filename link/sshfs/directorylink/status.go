package directorylink

import (
	"fmt"
)

func (dl *DirectoryLink) Status() string {
	health := "FAIL"

	if dl.Health() {
		health = "OK"
	}

	s := fmt.Sprintf("%s:%s %s", dl.Local, dl.Remote, health)

	return s
}
