package directorylink

import (
	"log"
	"time"

	"./linksocket"
)

const (
	// How many failed link socket health checks before reporting unhealthy.
	LINKSOCKET_HEALTH_TRIES = 3

	// How long to wait between failed link socket health checks.
	LINKSOCKET_FAIL_INTERVAL = 1 * time.Second
)

func (dl *DirectoryLink) Health() bool {
	for i := 0; i < LINKSOCKET_HEALTH_TRIES; i++ {
		if err := linksocket.MountHealthCheck(dl.Local); err != nil {
			log.Printf("linksocket healthcheck failed: %v", err)
			time.Sleep(LINKSOCKET_FAIL_INTERVAL)
			continue
		}
		return true
	}
	return false
}
