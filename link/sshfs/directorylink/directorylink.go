package directorylink

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"time"

	"../../../unix"
	"./linksocket"
)

const (
	PORT = "22"
	USER = "link"
)

type DirectoryLink struct {
	Host         string
	Local        string
	Remote       string
	IdentityFile string
	log          *bytes.Buffer
	cmd          *exec.Cmd
}

func (dl *DirectoryLink) Clear() {
	if dl.cmd == nil {
		return
	}
	if err := dl.cmd.Process.Kill(); err != nil {
		log.Printf("Failed to clear previous directory link: %v", err)
	}
}

func (dl *DirectoryLink) Reset() error {
	log.Printf("Directory link reset for host %q", dl.Host)
	dl.Clear()
	if err := dl.Verify(); err != nil {
		return fmt.Errorf("verify: %v", err)
	}

	host := dl.Host
	remote := dl.Remote
	local := dl.Local

	identityFile := dl.IdentityFile

	cmd := unix.Command("sshfs",
		fmt.Sprintf("%s@%s:%s",
			USER,
			host,
			remote,
		),
		local,
	)

	cmd.Opt("-p", PORT)
	cmd.Opt("-o", strings.Join(
		[]string{
			"allow_other",
			"reconnect",
		}, ","),
	)
	if identityFile != "" {
		cmd.Opt("-o",
			fmt.Sprintf("IdentityFile=%s",
				identityFile,
			),
		)
	}

	b := &bytes.Buffer{}
	dl.log = b

	c, err := unix.Daemon(cmd, b, b)
	if err != nil {
		return fmt.Errorf("Start directory link %q: %v", host, err)
	}

	dl.cmd = c
	time.Sleep(1 * time.Second)

	if s := b.String(); s != "" {
		log.Printf("STDOUT: %s", s)
	}

	return nil
}

func (dl *DirectoryLink) Unmount() error {
	out, err := unix.Exec("fusermount", "-u", dl.Local)
	if err != nil {
		return fmt.Errorf("fusermount: %v", err)
	}

	if out != "" {
		log.Printf("STDOUT: %s", out)
	}
	return nil
}

func (dl *DirectoryLink) Verify() error {
	// Set up directory structure for link on both ends.

	// Unmount any previous mount.
	dl.Unmount()

	if err := linksocket.Create(dl.Local); err != nil {
		return err
	}

	if err := linksocket.VerifyRemote(dl.Host, dl.Remote); err != nil {
		return err
	}
	return nil
}

func New(host, local, remote string) *DirectoryLink {
	return &DirectoryLink{
		Local:  local,
		Remote: remote,
		Host:   host,
	}
}

func Init() error {
	if err := linksocket.Init(); err != nil {
		return err
	}
	return nil
}
