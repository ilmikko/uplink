package sshfs

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func parseRule(rule string) (string, string, error) {
	fields := strings.Split(rule, ":")

	if len(fields) < 2 {
		return "", "", fmt.Errorf("Invalid rule: %q", rule)
	}

	return fields[0], fields[1], nil
}

func parseSSHFSRules(r io.Reader) (*SSHFS, error) {
	sshfs, err := New()
	if err != nil {
		return nil, err
	}

	s := bufio.NewScanner(r)
	for s.Scan() {
		fields := strings.Fields(s.Text())

		// Lines need to have at least hostname and a single rule.
		if len(fields) < 2 {
			continue
		}

		// Comments
		if fields[0][0] == '#' {
			continue
		}

		host := fields[0]
		rules := fields[1:]
		for _, rule := range rules {
			local, remote, err := parseRule(rule)
			if err != nil {
				return nil, err
			}
			if err := sshfs.Add(host, local, remote); err != nil {
				return nil, err
			}
		}
	}

	return sshfs, nil
}

func Load(cfg string) (*SSHFS, error) {
	if cfg == "" {
		return New()
	}

	f, err := os.Open(cfg)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	s, err := parseSSHFSRules(f)
	if err != nil {
		return nil, err
	}

	return s, nil
}
