package sshfs

import (
	"fmt"
	"log"
	"path/filepath"

	"../../config"
	"./directorylink"
)

var (
	IDENTITY_FILE = ""
	SSHFS_MODULES = []*SSHFS{}
)

type SSHFS struct {
	Hosts        map[string][]*directorylink.DirectoryLink
	IdentityFile string
}

func (s *SSHFS) Add(host, local, remote string) error {
	dls, ok := s.Hosts[host]
	if !ok {
		dls = []*directorylink.DirectoryLink{}
		s.Hosts[host] = dls
	}

	dl := directorylink.New(host, local, remote)
	dl.IdentityFile = s.IdentityFile

	s.Hosts[host] = append(dls, dl)
	return nil
}

func (s *SSHFS) Setup() error {
	for host, dls := range s.Hosts {
		for _, dl := range dls {
			if err := dl.Reset(); err != nil {
				log.Printf("DirectoryLink reset host %q: %v", host, err)
			}
		}
	}
	return nil
}

func New() (*SSHFS, error) {
	identityFileAbs, err := filepath.Abs(IDENTITY_FILE)
	if err != nil {
		return nil, err
	}

	s := &SSHFS{
		Hosts:        map[string][]*directorylink.DirectoryLink{},
		IdentityFile: identityFileAbs,
	}

	// This is required because we don't want a goroutine for every SSHFS module.
	SSHFS_MODULES = append(SSHFS_MODULES, s)

	return s, nil
}

func Init(cfg *config.Config) error {
	// TODO: Why is this here?
	IDENTITY_FILE = cfg.PrivateKeyFile()
	if err := directorylink.Init(); err != nil {
		return fmt.Errorf("directorylink: %v", err)
	}
	return nil
}
