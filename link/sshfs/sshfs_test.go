package sshfs_test

import (
	"reflect"
	"testing"

	sshfs "."
)

func TestLoadEmpty(t *testing.T) {
	s, err := sshfs.Load("")
	if err != nil {
		t.Errorf("Load: %v", err)
	}

	empty, err := sshfs.New()
	if err != nil {
		t.Fatalf("New: %v", err)
	}
	if want, got := empty, s; !reflect.DeepEqual(want, got) {
		t.Errorf("Empty SSHFS not as expected!\n got: %v\nwant: %v", got, want)
	}
}
