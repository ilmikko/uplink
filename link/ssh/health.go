package ssh

import (
	"log"

	"./health"
)

func (s *SSH) Health(host string) bool {
	stat := health.CheckHost(host)
	if stat.IsError() {
		if out, ok := s.HOSTS_STDOUT[host]; ok {
			log.Printf("Healthcheck failure for %q: %s", host, out)
		}
		return false
	}
	return true
}

func (s *SSH) HealthAll() bool {
	for host := range s.HOSTS_LINKS {
		if !s.Health(host) {
			log.Printf("SSH host %q reporting unhealthy", host)
			return false
		}
	}
	return true
}

func (s *SSH) Repair(host string) bool {
	if s.Health(host) {
		return true
	}

	s.Reset(host)
	return true
}

func (s *SSH) RepairAll() (success bool) {
	success = true
	for host := range s.HOSTS_LINKS {
		if !s.Repair(host) {
			success = false
		}
	}
	return success
}
