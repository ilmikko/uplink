package ssh

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"time"

	"../../comm/port"
	"../../config"
	"../../forward"
	"../hosts"
	"./health"
)

const (
	CONNECT_TIMEOUT = 60 * time.Second
	CONNECT_TRIES   = 5
)

var (
	CFG         *config.Config
	SSH_MODULES = []*SSH{}
)

type SSH struct {
	HOSTS_LINKS  map[string]*exec.Cmd
	HOSTS_STDOUT map[string]*bytes.Buffer
}

func (s *SSH) Clear(host string) {
	health.Unregister(host)
	if link, ok := s.HOSTS_LINKS[host]; ok {
		if err := link.Process.Kill(); err != nil {
			log.Printf("Failed to clear previous link: %v", err)
		}
	}
}

func (s *SSH) Link(host, key string) error {
	if key != "" {
		if err := hosts.Trust(host, key); err != nil {
			return err
		}
	}
	if err := s.Reset(host); err != nil {
		return err
	}
	return nil
}

func (s *SSH) Reset(host string) error {
	log.Printf("Link reset for host %q...", host)
	s.Clear(host)

	cmd := exec.Command(
		"ssh", "-N",
		fmt.Sprintf("%s@%s", CFG.User, host),
		"-i", CFG.PrivateKeyFile(),
		"-L", fmt.Sprintf("%v:localhost:%v",
			port.For(host),
			CFG.SelfPort,
		),
		strings.Join(forward.RulesString(host), " "),
	)

	b := &bytes.Buffer{}
	cmd.Stdout = b
	cmd.Stderr = b
	s.HOSTS_STDOUT[host] = b

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("Start host link %s: %v", host, err)
	}

	health.Register(host)
	s.HOSTS_LINKS[host] = cmd
	for i := 0; i < CONNECT_TRIES; i++ {
		if s.Health(host) {
			break
		}
		time.Sleep(CONNECT_TIMEOUT / CONNECT_TRIES)
	}

	if !s.Health(host) {
		return fmt.Errorf("Host did not connect in time.")
	}

	log.Printf("Host connected successfully")
	return nil
}

func (s *SSH) Setup() error {
	if len(hosts.HOSTS) == 0 {
		return fmt.Errorf("No hosts configured. Waiting for connections...")
	}

	for host := range hosts.HOSTS {
		if err := s.Link(host, ""); err != nil {
			log.Printf("SSH Link host %q: %v", host, err)
		}
	}
	return nil
}

func New() (*SSH, error) {
	s := &SSH{
		HOSTS_LINKS:  map[string]*exec.Cmd{},
		HOSTS_STDOUT: map[string]*bytes.Buffer{},
	}

	SSH_MODULES = append(SSH_MODULES, s)

	return s, nil
}

func Init(cfg *config.Config) error {
	CFG = cfg
	if err := health.Init(cfg); err != nil {
		return fmt.Errorf("health: %v", err)
	}

	return nil
}
