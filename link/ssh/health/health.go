package health

import (
	"fmt"
	"log"
	"strings"
	"time"

	"../../../config"
	"../../hosts"
	"./status"
)

const (
	HEALTHY       = "OK"
	MAX_FAILURES  = 5               // Reset connection after this many failed health checks.
	CHECK_TIMEOUT = 5 * time.Second // How long to wait between retrying failed health checks?
)

var (
	HOSTS_FAILURES     = map[string]int{}
	HOSTS_HEALTHCHECKS = map[string]time.Time{}
	CFG                *config.Config
)

func Status() string {
	return fmt.Sprintf(
		"%s %s",
		HEALTHY,
		CFG.SelfAddress,
	)
}

func Check(host string) *status.Status {
	res, err := hosts.Health(host)
	if err != nil {
		// Too much log spam for such a low level feature.
		// log.Printf("Error: Host health check: %v", err)
		return status.UNHEALTHY
	}
	if res == "" {
		return status.UNHEALTHY
	}

	fields := strings.Fields(res)
	if fields[0] != "OK" {
		return status.UNHEALTHY
	}

	if len(fields) > 1 {
		// Verify we are talking to the correct host if possible.
		if fields[1] != host {
			log.Printf("Wrong host response for host %q (%q)!", host, fields[1])
			return status.UNHEALTHY
		}
	}

	if !isLinkHealthy(host) {
		return status.SINGLE
	}

	return status.HEALTHY
}

func isRegistered(host string) bool {
	if _, ok := HOSTS_FAILURES[host]; ok {
		return true
	}
	return false
}

func Unregister(host string) {
	status.Update(host, status.UNREACHABLE)
	delete(HOSTS_FAILURES, host)
}

func Register(host string) {
	status.Update(host, status.CONNECTING)
	HOSTS_FAILURES[host] = 0
}

func CheckHost(host string) *status.Status {
	if !isRegistered(host) {
		return status.UNKNOWN
	}

	var stat *status.Status

	for {
		stat = Check(host)
		if !stat.IsError() {
			HOSTS_FAILURES[host] = 0
			hosts.Add(host)
			break
		}
		HOSTS_FAILURES[host]++

		if HOSTS_FAILURES[host] >= MAX_FAILURES {
			log.Printf("Link missing for %q after %d failures.", host, HOSTS_FAILURES[host])
			stat = status.MISSING
			break
		}
		time.Sleep(CHECK_TIMEOUT)
	}

	status.Update(host, stat)

	switch stat {
	case status.SINGLE:
		if !CFG.SingleLink {
			// If our side of the double link is good but theirs is bad, report OK for our healthcheck
			// but request them to reset their link.
			if _, err := hosts.Link(host); err != nil {
				log.Printf("Failed to link host %q: %v", host, err)
			}
		}
	}

	return stat
}

func isLinkHealthy(host string) bool {
	// Check if double link is healthy.
	// This is done by assuming that a recent healthcheck from that host came through.
	last, ok := HOSTS_HEALTHCHECKS[host]
	if !ok {
		// Never received a healthcheck.
		return false
	}

	delta := time.Now().Sub(last)
	return delta < time.Hour
}

func Init(cfg *config.Config) error {
	CFG = cfg
	httpInit()
	return nil
}
