package health_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	health "."
	"../../../comm"
	"../../../config"
)

const (
	testHost = "test-host"
)

func TestHealthCheck(t *testing.T) {
	testMux := http.NewServeMux()

	comm.OverrideMux(testMux)

	cfg := config.New()
	cfg.SelfAddress = testHost

	health.Init(cfg)

	server := httptest.NewServer(testMux)
	defer server.Close()

	resp, err := http.Get(server.URL + "/_health/test")
	if err != nil {
		t.Errorf("Unknown error: %v", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("Unknown error: %v", err)
	}

	want := fmt.Sprintf("%s %s",
		"OK",
		testHost,
	)

	if got, want := string(body), want; got != want {
		t.Errorf("Healthcheck response should be %q, but was %q",
			want,
			got,
		)
	}
}
