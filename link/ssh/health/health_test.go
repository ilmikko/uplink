package health_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	health "."
	"../../../comm"
	"../../../config"
	"../../hosts"
	"./status"
)

func TestUnhealthyHost(t *testing.T) {
	dir, err := ioutil.TempDir("", "TestUnhealthyHost")
	if err != nil {
		t.Fatalf("Cannot make temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	TEST_STATUS := ""

	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "%s", TEST_STATUS)
		}),
	)
	defer ts.Close()

	testMux := http.NewServeMux()
	comm.OverrideMux(testMux)
	comm.OverrideURL(ts.URL)
	comm.RegisterDefaults()

	cfg := config.TestDir(dir)
	cfg.SelfAddress = "testing"

	if err := hosts.Init(cfg); err != nil {
		t.Errorf("hosts.Init: %v", err)
	}
	if err := health.Init(cfg); err != nil {
		t.Errorf("health.Init: %v", err)
	}

	server := httptest.NewServer(testMux)
	defer server.Close()

	testCases := []struct {
		status string
		want   *status.Status
	}{
		{
			status: "FAIL unhealthy-host",
			want:   status.UNHEALTHY,
		},
		{
			status: "UNKNOWN unhealthy-host",
			want:   status.UNHEALTHY,
		},
		{
			status: "OK wrong-host",
			want:   status.UNHEALTHY,
		},
	}

	for _, tc := range testCases {
		TEST_STATUS = tc.status
		got := health.Check("unhealthy-host")
		want := tc.want
		if got != want {
			t.Errorf("Status not as expected:\n got: %s\nwant: %s", got.String(), want.String())
		}
	}
}

func TestHealthStatus(t *testing.T) {
	TEST_STATUS := ""

	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, TEST_STATUS)
		}),
	)
	defer ts.Close()

	comm.OverrideURL(ts.URL)

	health.Register("test-host")
	if stat, want := status.For("test-host"), status.CONNECTING; stat != want {
		t.Errorf("Status should be %s after registering, but was %s",
			want.String(),
			stat.String(),
		)
	}

	TEST_STATUS = ""
	if stat, want := health.Check("test-host"), status.UNHEALTHY; stat != want {
		t.Errorf("Status should be %s when host is unhealthy, but was %s",
			want.String(),
			stat.String(),
		)
	}

	TEST_STATUS = health.HEALTHY
	if stat, want := health.Check("test-host"), status.SINGLE; stat != want {
		t.Errorf("Status should be %s for an anonymous healthy host, but was %s",
			want.String(),
			stat.String(),
		)
	}

	TEST_STATUS = fmt.Sprintf("%s test-host", health.HEALTHY)
	if stat, want := health.Check("test-host"), status.SINGLE; stat != want {
		t.Errorf("Status should be %s when host is healthy, but was %s",
			want.String(),
			stat.String(),
		)
	}

	health.Unregister("test-host")
	if stat, want := status.For("test-host"), status.UNREACHABLE; stat != want {
		t.Errorf("Status should be %s after unregistering, but was %s",
			want.String(),
			stat.String(),
		)
	}
}

func TestHealthyDoubleLink(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, health.HEALTHY)
		}),
	)
	defer ts.Close()
	comm.OverrideURL(ts.URL)

	testMux := http.NewServeMux()
	comm.OverrideMux(testMux)
	comm.RegisterDefaults()

	cfg := config.New()

	if err := health.Init(cfg); err != nil {
		t.Errorf("health.Init: %v", err)
	}

	server := httptest.NewServer(testMux)
	defer server.Close()

	if stat, want := health.Check("test-host"), status.SINGLE; stat != want {
		t.Errorf("Status should be %s when singly linked, but was %s",
			want.String(),
			stat.String(),
		)
	}

	_, err := http.Get(server.URL + "/_health/test-host")
	if err != nil {
		t.Errorf("Unknown error: %v", err)
	}

	if stat, want := health.Check("test-host"), status.HEALTHY; stat != want {
		t.Errorf("Status should be %s when doubly linked, but was %s",
			want.String(),
			stat.String(),
		)
	}
}
