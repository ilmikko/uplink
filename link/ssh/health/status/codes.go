package status

const (
	CODE_UNKNOWN     = iota // Did not initialize? Status not updated?
	CODE_CONNECTING         // Host is being (re-)linked.
	CODE_UNREACHABLE        // Host is unreachable due to an issue.
	CODE_SINGLE             // Host is singly linked.
	CODE_HEALTHY            // Host is reporting healthy.
	CODE_UNHEALTHY          // Host is reporting unhealthy.
	CODE_MISSING            // Host is missing / no reports.
)

var (
	UNKNOWN     = &Status{Code: CODE_UNKNOWN}
	CONNECTING  = &Status{Code: CODE_CONNECTING}
	UNREACHABLE = &Status{Code: CODE_UNREACHABLE}
	SINGLE      = &Status{Code: CODE_SINGLE}
	HEALTHY     = &Status{Code: CODE_HEALTHY}
	UNHEALTHY   = &Status{Code: CODE_UNHEALTHY}
	MISSING     = &Status{Code: CODE_MISSING}
)

func (status *Status) String() string {
	switch status {
	case CONNECTING:
		return "Connecting..."
	case SINGLE:
		return "[1;33mSingle[m"
	case HEALTHY:
		return "[1;32mHealthy[m"
	case UNHEALTHY:
		return "[1;31mUNHEALTHY[m"
	case UNREACHABLE:
		return "[1;31mUNREACHABLE[m"
	case MISSING:
		return "[1;31mMISSING[m"
	default:
		return "[1;33mUNKNOWN[m"
	}
}
