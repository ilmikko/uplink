package status_test

import (
	"testing"

	"."
)

func TestStatusUnknown(t *testing.T) {
	if got, want := status.For("unknown-host"), status.UNKNOWN; got != want {
		t.Errorf("Want status %s for unknown host but it was %s",
			want.String(),
			got.String(),
		)
	}
}

func TestSetGetStatus(t *testing.T) {
	status.Update("some-host", status.CONNECTING)
	if got, want := status.For("some-host"), status.CONNECTING; got != want {
		t.Errorf("Want status %s for connecting host but it was %s",
			want.String(),
			got.String(),
		)
	}

	status.Update("some-host", status.HEALTHY)
	if got, want := status.For("some-host"), status.HEALTHY; got != want {
		t.Errorf("Want status %s for healthy host but it was %s",
			want.String(),
			got.String(),
		)
	}
}
