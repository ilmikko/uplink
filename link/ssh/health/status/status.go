package status

var (
	HOSTS_STATUS = map[string]*Status{}
)

type Status struct {
	Code int
}

func (status *Status) IsError() bool {
	switch status {
	case UNKNOWN, UNHEALTHY, UNREACHABLE, MISSING:
		return true
	default:
		return false
	}
}

func (status *Status) IsConnected() bool {
	switch status {
	case HEALTHY, SINGLE:
		return true
	default:
		return false
	}
}

func For(host string) *Status {
	status, ok := HOSTS_STATUS[host]
	if ok {
		return status
	}
	return UNKNOWN
}

func Update(host string, status *Status) {
	HOSTS_STATUS[host] = status
}
