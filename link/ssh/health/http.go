package health

import (
	"log"
	"time"

	"../../../comm"
	"./status"
)

func healthCheckFrom(host string) {
	log.Printf("Healthcheck from %s", host)
	HOSTS_HEALTHCHECKS[host] = time.Now()
	if status.For(host) == status.SINGLE {
		status.Update(host, status.HEALTHY)
	}
}

func httpInit() {
	comm.RegisterArgs("_health", func(args []string) string {
		if len(args) == 0 {
			return ""
		}
		host := args[0]
		healthCheckFrom(host)
		return Status()
	})
}
