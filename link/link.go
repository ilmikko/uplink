package link

import (
	"fmt"
	"log"

	"../comm"
	"../config"
	"./health"
	"./ssh"
	"./sshfs"
)

var (
	SSHFS *sshfs.SSHFS
	SSH   *ssh.SSH
)

func setupRoutine() {
	if err := setup(); err != nil {
		log.Printf("Error: Setup: %v", err)
	}
}

func setup() error {
	if err := SSH.Setup(); err != nil {
		return fmt.Errorf("Error: SSH setup: %v", err)
	}
	if err := SSHFS.Setup(); err != nil {
		return fmt.Errorf("Error: SSHFS setup: %v", err)
	}

	health.Register(SSH)
	health.Register(SSHFS)
	return nil
}

func Init(cfg *config.Config) error {
	if err := ssh.Init(cfg); err != nil {
		return fmt.Errorf("ssh init: %v", err)
	}
	if err := sshfs.Init(cfg); err != nil {
		return fmt.Errorf("sshfs init: %v", err)
	}

	var err error
	SSH, err = ssh.New()
	if err != nil {
		return fmt.Errorf("SSH load: %v", err)
	}
	SSHFS, err = sshfs.Load(cfg.SshfsFile)
	if err != nil {
		return fmt.Errorf("SSHFS load: %v", err)
	}

	if err := health.Init(); err != nil {
		return fmt.Errorf("health.Init: %v", err)
	}

	comm.RegisterArgs("_link", func(args []string) string {
		if len(args) < 2 {
			return ""
		}

		host := args[0]
		key := args[1]

		if err := SSH.Link(host, key); err != nil {
			log.Printf("Linking host %q failed: %v", host, err)
			return "FAIL"
		}
		return "OK"
	})

	go setupRoutine()
	return nil
}
